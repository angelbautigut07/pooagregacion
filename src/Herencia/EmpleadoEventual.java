/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author angel
 */
public class EmpleadoEventual extends Empleado {

    private float pagoHora;
    private float horasTrabajadas;

    public EmpleadoEventual() {
        this.pagoHora = 0.0f;
        this.horasTrabajadas = 0.0f;
    }

    public EmpleadoEventual(float pagoHora, float horasTrabajadas, int numEmpleado, String nombre, String puesto, String departamento) {
        super(numEmpleado, nombre, puesto, departamento);
        this.pagoHora = pagoHora;
        this.horasTrabajadas = horasTrabajadas;
    }

    public float getPagoHora() {
        return pagoHora;
    }

    public void setPagoHora(float pagoHora) {
        this.pagoHora = pagoHora;
    }

    public float getHorasTrabajadas() {
        return horasTrabajadas;
    }

    public void setHorasTrabajadas(float horasTrabajadas) {
        this.horasTrabajadas = horasTrabajadas;
    }

    @Override
    public float calcularPago() {
        return this.horasTrabajadas * this.pagoHora;
    }

}
