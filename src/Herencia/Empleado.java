/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author angel
 */
public abstract class Empleado {
    protected int numEmpleado;
    protected String nombre;
    protected String puesto;
    protected String departamento;
    
       public Empleado() {
        this.numEmpleado = 0;
        this.nombre = null;
        this.puesto = null;
        this.departamento = null;
    }

    public Empleado(int numEmpleado, String nombre, String puesto, String departamento) {
        this.numEmpleado = numEmpleado;
        this.nombre = nombre;
        this.puesto = puesto;
        this.departamento = departamento;
    }

    public int getNumEmpleado() {
        return numEmpleado;
    }

    public void setNumEmpleado(int numEmpleado) {
        this.numEmpleado = numEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }
    
    public abstract float calcularPago();
}
