/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author angel
 */
public class EmpleadoBase extends Empleado implements Impuesto {

    private float diasTrabajados;
    private float pagoDia;

    public EmpleadoBase() {
        this.diasTrabajados = 0.0f;
        this.pagoDia = 0.0f;
    }

    public EmpleadoBase(float diasTrabajados, float pagoDia, int numEmpleado, String nombre, String puesto, String departamento) {
        super(numEmpleado, nombre, puesto, departamento);
        this.diasTrabajados = diasTrabajados;
        this.pagoDia = pagoDia;
    }

    public float getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(float diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }

    public float getPagoDia() {
        return pagoDia;
    }

    public void setPagoDia(float pagoDia) {
        this.pagoDia = pagoDia;
    }

    @Override
    public float calcularPago() {
        return this.diasTrabajados * this.pagoDia;
    }

    @Override
    public float calcularImpuesto() {
        float impuestos = 0.0f;
        if (this.calcularPago() > 5000) {
            impuestos = this.calcularPago() * .16f;
        }
        return impuestos;
    }
}
