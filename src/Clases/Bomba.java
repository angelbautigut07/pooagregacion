/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author angel
 */
public class Bomba {
    private int numBomba;
    private float contador;
    private float capacidad;
    private Gasolina gasolina;

    public Bomba() {
         this.numBomba = 0;
        this.contador = 0.0f;
        this.capacidad = 0.0f;
        this.gasolina = new Gasolina();
    }

    public Bomba(int numBomba, float contador, float capacidad, Gasolina gasolina) {
        this.numBomba = numBomba;
        this.contador = contador;
        this.capacidad = capacidad;
        this.gasolina = gasolina;
    }
    
        public Bomba(Bomba x) {
         this.numBomba = x.numBomba;
        this.contador = x.contador;
        this.capacidad = x.capacidad;
        this.gasolina = x.gasolina;
    }

    public int getNumBomba() {
        return numBomba;
    }

    public void setNumBomba(int numBomba) {
        this.numBomba = numBomba;
    }

    public float getContador() {
        return contador;
    }

    public void setContador(float contador) {
        this.contador = contador;
    }

    public float getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(float capacidad) {
        this.capacidad = capacidad;
    }

    public Gasolina getGasolina() {
        return gasolina;
    }

    public void setGasolina(Gasolina gasolina) {
        this.gasolina = gasolina;
    }
        
    public float obtenerInventario(){
        return this.capacidad - this.contador;
    }    
    
    public boolean realizarVenta(float cantidad){
        boolean exito = false;
        if (cantidad <= this.obtenerInventario()) {
            exito = true;
    }
        return exito;
    }
    
    public float calcularTotal(){
        float total = 0.0f;
        total = (this.contador * gasolina.getPrecio());
        return total;
    }
}
