/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author angel
 */
public class Gasolina {
    private float precio;
    private int id;
    private int tipo;
    private String marca;

    public Gasolina(float precio, int id, int tipo, String marca) {
        this.precio = precio;
        this.id = id;
        this.tipo = tipo;
        this.marca = marca;
    }

    public Gasolina() {
        this.precio = 0.0f;
        this.id = 0;
        this.tipo = 0;
        this.marca = "No se ha determinado";
    }
    
    public Gasolina(Gasolina x) {
        this.precio = x.precio;
        this.id = x.id;
        this.tipo = x.tipo;
        this.marca = x.marca;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String mostrarInformacion(){
        String info ="";
        info = ("Id: " + this.id + " Tipo: " + this.tipo + " Marca: " + this.marca + " Precio: $" + this.precio);
        return info;
    }
    
}
